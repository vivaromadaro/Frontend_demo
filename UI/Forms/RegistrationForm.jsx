import styles from './form.module.scss'

const RegistrationForm = ({ type, text, value, onChange }) => {
	return (
		<div className={styles.formField}>
			<label htmlFor={type}>{text}</label>
			<input
				type={type}
				id={type}
				name={type}
				value={value}
				onChange={onChange}
				placeholder={
					type === 'text'
						? `Введіть ваше ${text.toLowerCase()}`
						: `Введіть ${text.toLowerCase()}`
				}
				required
			/>
		</div>
	)
}

export default RegistrationForm
