import styles from './form.module.scss'

const LayoutForm = ({ onSubmit, children, text }) => {
	return (
		<div className={styles.formPage}>
			<form className={styles.formContainer} onSubmit={onSubmit}>
				{children}
				{text ? (
					<button className={styles.submitButton} type='submit' text={text}>
						{text}
					</button>
				) : null}
			</form>
		</div>
	)
}

export default LayoutForm
