import Link from 'next/link'
import styles from './header.module.scss'


const SubMenu = ({ way, text }) => {
	return (
		<li>
			<Link href={`/${way}`}>{text}</Link>
		</li>
	)
}

export default SubMenu
