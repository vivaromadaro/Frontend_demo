import styles from './header.module.scss'

const LayoutSubMenu = ({ children }) => {
	return <ul className={styles.subMenu}>{children}</ul>
}

export default LayoutSubMenu
