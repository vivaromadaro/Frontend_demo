import Link from 'next/link'
import SubMenu from './SubMenu'
import styles from './header.module.scss'
import LayoutSubMenu from './LayoutSubMenu'

const Header = () => {
	return (
		<header className={styles.header}>
			<nav>
				<ul className='flex'>
					<li>
						<Link href={'/'}>Home</Link>
					</li>
					<li>
						<Link href={'/articles'}>Articles</Link>
						<LayoutSubMenu>
							<SubMenu text={'Categories'} way={'/gallery'} />
						</LayoutSubMenu>
					</li>
					<li>
						<Link href={'/pagination'}>Pagination</Link>
					</li>
					<li>
						<Link href={'/authorization'}>Authorization</Link>
					</li>
				</ul>
			</nav>
		</header>
	)
}

export default Header
