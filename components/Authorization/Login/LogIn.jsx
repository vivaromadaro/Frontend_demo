import RegistrationForm from '@/UI/Forms/RegistrationForm'
import LayoutForm from '@/UI/Forms/layoutForm'
import Heading from '@/components/Header/Heading'
import { useState } from 'react'
import styles from '../../../UI/Forms/form.module.scss'

const Login = () => {
	const [formData, setFormData] = useState({
		email: '',
		password: '',
	})
	const handleChange = e => {
		const { name, value } = e.target
		setFormData({
			...formData,
			[name]: value,
		})
	}
	const handleSubmit = e => {
		e.preventDefault()
		// Here, you can handle the form submission, e.g., send data to your server.
		setFormData({
			firstName: '',
			lastName: '',
			password: '',
			email: '',
		})
		console.log(formData)
	}
	return (
		<LayoutForm onSubmit={handleSubmit} text='Увійти'>
			<div className={styles.formField}>
				<Heading text={'Вхід'} tag={'h2'} />
				<RegistrationForm
					type={'email'}
					text={'Email:'}
					value={formData.email}
					onChange={handleChange}
				/>
				<RegistrationForm
					type={'password'}
					text={'Пароль:'}
					value={formData.password}
					onChange={handleChange}
				/>
				<div className={styles.checkboxContainer}>
					<input type='checkbox' />
					<label>Запам'ятати мене</label>
				</div>
				
			</div>
		</LayoutForm>
	)
}

export default Login
