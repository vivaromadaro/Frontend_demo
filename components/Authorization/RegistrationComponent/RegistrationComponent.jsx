import RegistrationForm from '@/UI/Forms/RegistrationForm'
import LayoutForm from '@/UI/Forms/layoutForm'
import Heading from '@/components/Header/Heading'
import { useState } from 'react'

// const StateRegistration = () => {
// 	const [formData, setFormData] = useState({
// 		firstName: '',
// 		lastName: '',
// 		password: '',
// 		email: '',
// 	})

// 	const handleSubmit = e => {
// 		e.preventDefault()
// 		setFormData({
// 			firstName: '',
// 			lastName: '',
// 			password: '',
// 			email: '',
// 		})
// 		console.log(formData)
// 	}

// 	const handleChange = e => {
// 		const { name, value } = e.target
// 		setFormData({
// 			...formData,
// 			[name]: value,
// 		})
// 	}
// 	return [formData, setFormData, handleChange, handleSubmit]
// }

const RegistrationComponent = () => {
	// const [formData, setFormData, handleChange, handleSubmit] = StateRegistration()

	const [formData, setFormData] = useState({
		firstName: '',
		lastName: '',
		password: '',
		email: '',
	})

	const handleSubmit = e => {
		e.preventDefault()
		setFormData({
			firstName: '',
			lastName: '',
			password: '',
			email: '',
		})
		console.log(formData)
	}

	const handleChange = e => {
		const { name, value } = e.target
		setFormData({
			...formData,
			[name]: value,
		})
	}

	return (
		<LayoutForm onSubmit={handleSubmit} text='Зареєструватися'>
			<Heading tag={'h2'} text={'Реєстрація'} />
			<RegistrationForm
				type={'text'}
				text={"Ім'я"}
				value={formData.firstName}
				onChange={e =>
					setFormData({
						...formData,
						firstName: e.target.value,
					})
				}
			/>
			<RegistrationForm
				type={'text'}
				text={'Прізвище'}
				value={formData.lastName}
				onChange={e =>
					setFormData({
						...formData,
						lastName: e.target.value,
					})
				}
			/>
			<RegistrationForm
				type={'password'}
				text={'Пароль'}
				value={formData.password}
				onChange={handleChange}
			/>
			<RegistrationForm
				type={'email'}
				text={'Email'}
				value={formData.email}
				onChange={handleChange}
			/>
		</LayoutForm>
	)
}

export default RegistrationComponent
