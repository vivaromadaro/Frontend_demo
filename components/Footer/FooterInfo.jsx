const FooterInfo = ({title, text, linkTo}) => {
	return (
		<div>
			<h2>{title}</h2>
			<p>{text}</p>
			<a href='#'>{linkTo}</a>
		</div>
	)
}

export default FooterInfo
