import FooterInfo from './FooterInfo'
import styles from './footer.module.scss'

const Footer = () => {
	return (
		<footer className={styles.footer}>
			<>
				<FooterInfo title={'Заголовок Розділу 1'} text={'Текст розділу 1...'} linkTo={'Посилання 1'}/>
				<FooterInfo title={'Заголовок Розділу 2'} text={'Текст розділу 2...'} linkTo={'Посилання 2'}/>
				<FooterInfo title={'Заголовок Розділу 3'} text={'Текст розділу 3...'} linkTo={'Посилання 3'}/>
			</>
		</footer>
	)
}

export default Footer
