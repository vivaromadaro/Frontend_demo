import Image from 'next/image'
import styles from './categories.module.scss'
import Link from 'next/link'

const Categories = ({ titleArticle, articleDescription, id}) => {
	const numbers = [1, 2, 3]

	return (
		<div className={styles.article}>
			<Image
				className={styles.articleImage}
				src='/example.jpg'
				alt='Зображення статті'
				width={300}
				height={200}
			/>
			<h3 className={styles.articleTitle}> <Link href={`/articles/${id}`}>{titleArticle}{id}</Link> </h3>
			<p className={styles.ArticleDescription}>{articleDescription}{id}...</p>
			<div className={styles.articleTags}>
				{numbers.map(number => (
					<li className={styles.tag} key={number}>
						<span>
							Tag{number}
						</span>
					</li>
				))}
			</div>
		</div>
	)
}

export default Categories
