import LayoutForm from '@/UI/Forms/layoutForm'
import Link from 'next/link'

const Authorization = () => {
	return (
		<LayoutForm>
			<div className='flex flex-col text-center text-3xl'>
				<Link className='bg-orange-700 mb-6' href={'/authorization/signIn'}>
					SignIn
				</Link>
				<Link className='bg-orange-700' href={'/authorization/registration'}>
					Registration
				</Link>
			</div>
		</LayoutForm>
	)
}

export default Authorization
