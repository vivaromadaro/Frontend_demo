import Categories from '@/components/Categories/Categories'
import styles from '../components/Categories/categories.module.scss'

const Gallery = () => {
	return (
		<div className={styles.articlesContainer}>
			<Categories id={1} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
			<Categories id={2} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
			<Categories id={3} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
			<Categories id={4} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
			<Categories id={5} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
			<Categories id={6} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
			<Categories id={7} titleArticle={'Стаття #'} articleDescription={'Опис статті #'}/>
		</div>
	)
}

export default Gallery
